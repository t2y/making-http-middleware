# making-http-middleware

## 実行方法

http サーバーを起動する

```
$ go run main.go
2024/03/28 10:56:36 start http server with 8080 port
```

起動した http サーバーのルートパス (`/`) へリクエストするとユーザーエージェントの値が返ってくる

```
$ curl localhost:8080/
UserAgent is curl/7.81.0
```

サーバーのログから次のようにミドルウェアやハンドラーの呼び出しを確認する

```
2024/03/28 10:56:38  - middlewareOne: before
2024/03/28 10:56:38    - middlewareTwo: before
2024/03/28 10:56:38       - Called root handler
2024/03/28 10:56:38    - middlewareTwo: after
2024/03/28 10:56:38  - middlewareOne: after
```

今度はルートパス (`/`) ではなく `/hello` のエンドポイントへリクエストするとレスポンスが返ってこない

```
$ curl localhost:8080/hello
$ # NO RESPONSE 
```

サーバーのログでは middlewareTwo の before 以降の処理が呼ばれていないことを確認できる

```
2024/03/28 11:02:17  - middlewareOne: before
2024/03/28 11:02:17    - middlewareTwo: before
2024/03/28 11:02:17  - middlewareOne: after
```

## リファレンス

* [HTTP Middleware の作り方と使い方](https://tutuz-tech.hatenablog.com/entry/2020/03/23/220326)
* [アスペクト指向プログラミング (AOP)](https://ja.wikipedia.org/wiki/%E3%82%A2%E3%82%B9%E3%83%9A%E3%82%AF%E3%83%88%E6%8C%87%E5%90%91%E3%83%97%E3%83%AD%E3%82%B0%E3%83%A9%E3%83%9F%E3%83%B3%E3%82%B0)
