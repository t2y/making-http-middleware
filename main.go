package main

import (
	"fmt"
	"log"
	"net/http"
)

func middlewareOne(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Println(" - middlewareOne: before")
		next.ServeHTTP(w, r)
		log.Println(" - middlewareOne: after")
	})
}

func middlewareTwo(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Println("   - middlewareTwo: before")
		if r.URL.Path != "/" {
			return
		}
		next.ServeHTTP(w, r)
		log.Println("   - middlewareTwo: after")
	})
}

func root(w http.ResponseWriter, r *http.Request) {
	log.Println("      - Called root handler")
	message := fmt.Sprintf("UserAgent is %s\n", r.UserAgent())
	w.Write([]byte(message))
}

func main() {
	port := "8080"
	rootHandler := http.HandlerFunc(root)
	http.Handle("/", middlewareOne(middlewareTwo(rootHandler)))
	log.Printf("start http server with %s port\n", port)
	if err := http.ListenAndServe(fmt.Sprintf(":%s", port), nil); err != nil {
		log.Fatalln(err)
	}
}
